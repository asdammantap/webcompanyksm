<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class First extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function index()
	{
		$this->load->library('googlemaps');
     
    $config['center'] = '-6.344314,106.8071701';//Jaksel
	$config['center1'] = '-6.3722281,106.889324';//wiladatika
	$config['center2'] = '-6.1940542,106.6316924';//Tangerang
	$config['center3'] = '-6.3627622,106.8255594';//Tangerang
    //$config['zoom'] = 'auto';
    $this->googlemaps->initialize($config);
     
    $marker = array();
    $marker['position'] = '-6.344314,106.8071701';
    $this->googlemaps->add_marker($marker);
	$marker1['position'] = '-6.3722281,106.889324';
    $this->googlemaps->add_marker($marker1);
	$marker2['position'] = '-6.1940542,106.6316924';
    $this->googlemaps->add_marker($marker2);
	$marker3['position'] = '-6.3627622,106.8255594';
    $this->googlemaps->add_marker($marker3);
     
		$data['map'] = $this->googlemaps->create_map();
		$this->load->view('index',$data);
	}
}