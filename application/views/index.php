<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="profile PT. KSM">
	<meta name="keyword" content="ksm, profile, company profile, toner">
	<meta name="author" content="Wong Mantap">
    <title>PT. Kreasi Sarana Mandiri</title>
	
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="assets/css/prettyPhoto.css" rel="stylesheet">      
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IMR-aPaaVAFJ548fI2bKUexnzy5aXyA&callback=initMap"
  type="text/javascript"></script>
	<link rel="icon" type="image/png" id="favicon"
          href="assets/images/ksm-logo.png"/>
    <!-- =======================================================
        Theme Name: Gp
        Theme URL: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-templat/
        Author: BootstrapMade
        Author URL: https://bootstrapmade.com
    ======================================================= -->  
    <?php echo $map['js']; ?>
  </head>
  <body class="homepage"> 
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9164800;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->  
	<header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="main"><img src="assets/images/ksm-logo5.png"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="">Home</a></li>
                        <li><a href="">About Us</a></li>
                        <li><a href="">Services</a></li>
                        <li><a href="">Products</a></li>
                        <li><a href="">Contact Us</a></li>                        
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
	
	<div class="slider">
		<div class="container">
			<div id="about-slider">
				<div id="carousel-slider" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
				  	<ol class="carousel-indicators visible-xs">
					    <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-slider" data-slide-to="1"></li>
					    <li data-target="#carousel-slider" data-slide-to="2"></li>
				  	</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img src="assets/images/slider1tes.jpg" class="img-responsive" alt=""> 
					   </div>
					   <div class="item">
							<img src="assets/images/slider2tes.jpg" class="img-responsive" alt=""> 
					   </div> 
					   <div class="item">
							<img src="assets/images/slider1tes.jpg" class="img-responsive" alt=""> 
					   </div> 
					</div>
					
					<a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
						<i class="fa fa-angle-left"></i> 
					</a>
					
					<a class=" right carousel-control hidden-xs"href="#carousel-slider" data-slide="next">
						<i class="fa fa-angle-right"></i> 
					</a>
				</div> <!--/#carousel-slider-->
			</div><!--/#about-slider-->
		</div>
	</div>
	
	<section id="feature" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>WHO WE ARE</h2>
                <p class="lead">Perusahaan Nasional yang bergerak dibidang jasa penyediaan dan penyewaan mesin cetak Mutifungsi serta bahan pakainya.</br>
				KSM memiliki tenaga ahli dari generasi muda yang mempunyai semangat, kreativitas, dedikasi dan inovasi yang sangat tinggi</br> dengan bimbingan
				dan arahan dari para ahli dibidangnya.</br>
				<b>PT. KREASI SARANA MANDIRI didirikan sejak tanggal 01 agustus 2010 s/d saat ini</b></p>
            </div> 
			 
        <div class="center wow fadeInDown">
                <h2>KSM INOVATION PROGRAM</h2>
               
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-refresh"></i>
                            <h2>Reload Toner Program</h2>
                            <h3>Peminjaman mesin MFP Program</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-credit-card"></i>
                            <h2>Kredit Mesin</h2>
                            <h3>Soft loan payment</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-money"></i>
                            <h2>Penjualan Langsung</h2>
                            <h3>Cash / Outright sales</h3>
                        </div>
                    </div><!--/.col-md-4-->
                
                    
                </div><!--/.services-->
            </div><!--/.row-->    
        </div><!--/.container-->
    </section><!--/#feature-->
	
	 <section id="recent-works">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>AREA PELAYANAN DAN PENYEBARAN PELANGGAN</h2>
                
            </div>

            <div class="row">
                <?php echo $map['html']; ?>
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#recent-works-->	
	<div>&nbsp;</div>
    <section id="middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 wow fadeInDown">
                    <div class="skill">
                        <h2>Our Mission</h2>
                        <p>Komitmen kami untuk memberikan informasi dan solusi terbaik dalam membangun kerjasama solusi dokumen yang dapat memberikan efektivitas penghematan, kepuasan dan keuntungan melalui program-program yang handal dengan didukung kualitas teknologi dan pelayanan terbaik dalam dunia pendidikan di seluruh Indonesia.
						</p>

                        
                    </div>

                </div><!--/.col-sm-6-->
				<p></p>
                <div class="col-sm-6 wow fadeInDown">
                    <div class="accordion">
                        <h2>Testimoni</h2>
                        <div class="panel-group" id="accordion1">
                          <div class="panel panel-default">
                            <div class="panel-heading active">
                              <h3 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
                                  Pelayanannya Baik
                                  <i class="fa fa-angle-right pull-right"></i>
                                </a>
                              </h3>
                            </div>

                            <div id="collapseOne1" class="panel-collapse collapse in">
                              <div class="panel-body">
                                  <div class="media accordion-inner">
                                        <div class="pull-left">
                                            <img class="img-responsive" src="assets/images/accordion1.png">
                                        </div>
                                        <div class="media-body">
                                             <h4>Sutejo</h4>
                                             <p>Pelayanan yang diberikan begitu baik dan ramah.</p>
                                        </div>
                                  </div>
                              </div>
                            </div>
                          </div>

                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h3 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
                                  Response 24 Jam
                                  <i class="fa fa-angle-right pull-right"></i>
                                </a>
                              </h3>
                            </div>
                            <div id="collapseTwo1" class="panel-collapse collapse">
                              <div class="panel-body">
                                Responnya bagus banget dan 24 jam
                              </div>
                            </div>
                          </div>

                          
                        </div><!--/#accordion1-->
                    </div>
                </div>

            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#middle-->
	
	<section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Company</h3>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">We are hiring</a></li>
                            <li><a href="#">Meet the team</a></li>
                            <li><a href="#">Copyright</a></li>                           
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Support</h3>
                        <ul>
                            <li><a href="#">Faq</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Forum</a></li>
                            <li><a href="#">Documentation</a></li>                          
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Part of Our Consumer</h3>
                        <ul>
                            <li><a href="#">AEON STORE</a></li>
							<li><a href="#">STRADA</a></li>
							<li><a href="#">BANK BRI</a></li>
							<li><a href="#">PT. BALAI LELANG SEMPURNA</a></li>
							<li><a href="#">PT. TELKOM PROPERTY</a></li>
							<li><a href="#">BARESKRIM MABES POLRI</a></li>                 
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->
            </div>
        </div>
    </section><!--/#bottom-->
	
	<div class="top-bar">
		<div class="container">
			<div class="row">
			    <div class="col-lg-12">
				   <div class="social">
						<ul class="social-share">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
							<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
							<li><a href="#"><i class="fa fa-skype"></i></a></li>
						</ul>
				   </div>
                </div>
			</div>
		</div><!--/.container-->
	</div><!--/.top-bar-->
	
	<footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; PT. KSM All Rights Reserved.
                    <div class="credits">
                        <!-- 
                            All the links in the footer should remain intact. 
                            You can delete the links only if you purchased the pro version.
                            Licensing information: https://bootstrapmade.com/license/
                            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Gp
                        -->
                        <a href="https://bootstrapmade.com/">Designed</a> by <a href="">Kreasi Sarana Mandiri</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <script src="assets/js/jquery.isotope.min.js"></script>   
    <script src="assets/js/wow.min.js"></script>
	<script src="assets/js/main.js"></script>
    
</body>
</html>